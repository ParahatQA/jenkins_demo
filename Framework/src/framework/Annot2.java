package framework;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

public class Annot2 {

	public WebDriver driver = null;

	@Test
	public void Login() throws IOException {

		Properties prop = new Properties();
		FileInputStream fis = new FileInputStream(
				"C:\\Users\\Home\\eclipse-workspace\\Framework\\src\\framework\\datadriver.properties");

		prop.load(fis);

		if (prop.getProperty("browser").equals("chrome")) {

			driver = new ChromeDriver();

		} else if (prop.getProperty("browser").equals("firefox")) {

			driver = new FirefoxDriver();
		} else {
			driver = new InternetExplorerDriver();
		}

		driver.get(prop.getProperty("url"));

	}

}
