package framework;

import org.testng.annotations.Test;

public class DependencyAnnot {

	@Test
	public void openBrowser() {
		System.out.println("Executing opening Browser");
	}

	@Test(dependsOnMethods = { "openBrowser" }, alwaysRun = true)
	public void flightBooking() {
		System.out.println("Executing flightbooking");
	}

	@Test(timeOut = 45000)
	public void timeRelated() {
		System.out.println("New Testcase");
	}

	@Test(enabled = false)
	public void payment() {
		System.out.println("New Testcase");
	}

}
