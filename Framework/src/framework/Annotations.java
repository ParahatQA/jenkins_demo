package framework;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Annotations {

	@BeforeTest
	public void cookies() {
		System.out.println("This block executes before all testcases");
	}

	@AfterTest
	public void cookiesClose() {
		System.out.println("This block executes after all testcases");
	}
	
	
	@BeforeMethod
	public void userIdGeneration() {
		System.out.println("This  block executes before each Test");
	}

	

	@AfterMethod
	public void reportAdding() {

		System.out.println("This  block executes after each test");
	}

	@Test
	public void openBrowser() {
		System.out.println("Opening Browser");
	}

	@Test
	public void flightBooking() {
		System.out.println("Flight Booking");
	}

}
