package datadriven;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Annotations {
	
	@BeforeTest
	public void cookies() {
		System.out.println("This block executes before all testcases");
	}

	@AfterTest
	public void cookiesClose() {
		System.out.println("This block executes after all testcases");
	}
	
	
	@BeforeMethod
	public void userIdGeneration() {
		System.out.println("This  block executes before each Test");
	}
	
	@Test(dataProvider="getData")
	public void Userid(String username, String password, String id) {
		System.out.println("This  block executes before each test");
		System.out.println(username);
		System.out.println(password);
		System.out.println(id);
	}

	

	@AfterMethod
	public void reportAdding() {

		System.out.println("This  block executes after each test");
	}

	//@Test
	public void openBrowser() {
		System.out.println("Opening Browser");
	}

	//@Test
	public void flightBooking() {
		System.out.println("Flight Booking");
	}
	
	@DataProvider
	public Object[] [] getData(){
		//i stands  for number of times testcase should run
		//j stands for number of it should send for one go
		
		Object [] [] data= new Object[3] [3] ;
		
		data[0] [0] ="1abcd";
		data[0] [1]="1xyz";
		data[0] [2]="1abs";
		
		data[1] [0] ="2abcd";
		data[1] [1]="2xyz";
		data[1] [2]="2abs";
		
		data[2] [0] ="3abcd";
		data[2] [1]="3xyz";
		data[2] [2]="3abs";
		
		return data;
		
		
		
		
		
	}


}
