package demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ActionDemo {

	WebDriver driver;
	Actions action;

	@BeforeMethod
	public void setUp() {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Home\\Documents\\selenium dependencies\\drivers\\chromedriver.exe");

		driver = new ChromeDriver();

	}

	// @Test
	public void test() {

		driver.get("https://www.amazon.com/");

		action = new Actions(driver);
		action.moveToElement(driver.findElement(By.xpath("//span[.='Hello. Sign in']"))).build().perform();

		driver.findElement(By.linkText("Explore Idea Lists")).click();

	}

	// @Test
	public void dragAndDrop() {

		driver.get("https://demos.telerik.com/kendo-ui/dragdrop/index");
		action = new Actions(driver);

		WebElement source = driver.findElement(By.id("draggable"));
		WebElement target = driver.findElement(By.id("droptarget"));

		action.dragAndDrop(source, target).build().perform();

	}

	@Test
	public void doubleClick() {

		driver.get("https://www.primefaces.org/showcase/ui/misc/effect.xhtml");
		action = new Actions(driver);

		WebElement l = driver.findElement(By.id("fold_content"));

		action.doubleClick(l).build().perform();
	}

}
