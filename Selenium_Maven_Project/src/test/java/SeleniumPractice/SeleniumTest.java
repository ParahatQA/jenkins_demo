package SeleniumPractice;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SeleniumTest {

	
	
	WebDriver driver;
	
	@BeforeTest
	public void setUp() {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Home\\Documents\\selenium dependencies\\drivers\\chromedriver.exe");

	    driver = new ChromeDriver();
		driver.get("https://www.google.com/");

	}
	
	@AfterTest
	public void close() {
		driver.close();
	}

	
	
	@Test(priority=1)
	public void test2() {
 
		driver.findElement(By.xpath("//*[@id='hptl']/a[1]")).click();
	}

	@Test(priority=2)
	public void test1() {

		driver.findElement(By.linkText("Gmail")).click();

		String expected = "Gmail - Free Storage and Email from Google";
		String actual = driver.getTitle();

		System.out.println(driver.getTitle());

		Assert.assertEquals(actual, expected);

	}

}
