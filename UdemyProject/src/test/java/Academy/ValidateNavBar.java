package Academy;

import java.io.IOException;

import org.junit.Assert;
import org.testng.annotations.Test;

import Resources.Base;
import pageObjects.LandingPage;

public class ValidateNavBar extends Base {
	
	
	@Test
	public void basePageNavigation() throws IOException {
		
		driver=initializeDriver();
		driver.get(prop.getProperty("url"));
		
		LandingPage lp= new LandingPage(driver);
		Assert.assertTrue(lp.getNavigationBar().isDisplayed());
		

}
}