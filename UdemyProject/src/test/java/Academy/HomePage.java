package Academy;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Resources.Base;
import pageObjects.LandingPage;
import pageObjects.LoginPage;

public class HomePage extends Base {
	
	@Test(dataProvider="getData")
	public void basePageNavigation(String username,String password, String text) throws IOException {
		
		driver=initializeDriver();
		driver.get(prop.getProperty("url"));
		
		LandingPage lp= new LandingPage(driver);
		lp.getLogin().click();
		
		LoginPage loginpage= new LoginPage(driver);
		loginpage.getEmail().sendKeys(username);
		loginpage.getPassword().sendKeys(password);
		System.out.println(text);
		loginpage.clickButton().click();
	}
	@DataProvider
	public Object[][] getData() {
		//Row  stands for how many different data types test should run
		// Column stands for how  many values  per each test
		Object [] [] data= new Object[2][3];
		//0th row
		data[0][0]="nonrestricteduser@qw.com";
		data[0][1]="123456";
		data[0][2]="Restricted user";
	
		//1st row
		data[1][0]="nonrestricteduser@qw.com";
		data[1][1]="456789";
		data[1][2]="None Restricteduser@qw.com";
		
		return data;
		
		
		
	}
	

}
