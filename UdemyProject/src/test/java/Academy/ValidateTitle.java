package Academy;

import java.io.IOException;

import org.testng.annotations.Test;

import Resources.Base;
import junit.framework.Assert;
import pageObjects.LandingPage;

public class ValidateTitle extends Base {

	@Test
	public void basePageNavigation() throws IOException {
		
		driver=initializeDriver();
		driver.get(prop.getProperty("url"));
		// one is inheritance
		// creating object to that class and invoke methods of  it
		LandingPage lp= new LandingPage(driver);
		
		//compare the text from the browser with actual text.
		
		
		Assert.assertEquals(lp.getTitle().getText(), "FEATURED COURSES");
		
		System.out.println("ASSERTION  PASSED");
	}
}
