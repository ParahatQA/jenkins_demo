package lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Iterations {
	public static void main(String[] args) {

		List<String> jobIds = new ArrayList<>();
		jobIds.add("QA");
		jobIds.add("BA");
		jobIds.add("DEV");
		jobIds.add("PO");
		jobIds.add("SM");

		// List<String> jobIds2 =Arrays.asList("QA","DEV","BA");

		// System.out.println(jobIds);

		// System.out.println("=============FOR LOOP================");
		//
		// for (int i = 0; i <jobIds.size(); i++) {
		// System.out.println(jobIds.get(i));
		// }
		// System.out.println("=============FOR EACH================");
		//
		// for (String jobId : jobIds) {
		// System.out.println(jobId);
		// }
//				System.out.println("=============ITERATOR================");
//			
//				 Iterator<String> jobIdIterator=jobIds.iterator();
//				 while(jobIdIterator.hasNext()){
//				 System.out.println(jobIdIterator.next());
//				 }
		 System.out.println("=============ITERATOR MODIFY================");
		
		 Iterator<String> jobIdIterator = jobIds.iterator();
		 	
		 while (jobIdIterator.hasNext()) {
			String id = jobIdIterator.next();

			if (id.equals("PO")) {
				jobIdIterator.remove();
				// jobIds.add("PM");
			} else {
				System.out.println(id);

			}
		}
		System.out.println(jobIds);

//		 System.out.println("=============ITERATOR MODIFY================");
//		// BELOW CODE IS BAD :BECAUSE FOR EACH LOOP DOESNT LET TO MIDOFY
//		 for (String jobId : jobIds) {
//		 if(jobId.equals("SM")){
//		 jobIds.remove(jobId);
//		 }else{
//		 System.out.println(jobId);
//		 }
		// }
	}

}
