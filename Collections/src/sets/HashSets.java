package sets;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSets {
	
public static void main(String[] args) {
	
	HashSet<String> jobIds = new HashSet();
	
	jobIds.add("QA");
	jobIds.add("BA");
	jobIds.add("DEV");
	
	
	System.out.println(jobIds.add("SM"));  // it  was  added
	System.out.println(jobIds.add("QA"));  // it  was  not  added  because  it is  duplicated 
	System.out.println(jobIds.size());
	
	
	
	Iterator<String> it = jobIds.iterator();
	while(it.hasNext()){
		
		String id= it.next();
		System.out.println(id);
	}
	
	System.out.println(jobIds.remove("BA"));
	System.out.println(jobIds);
	
	
}

}
