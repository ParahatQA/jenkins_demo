package Day_Nov_25;

public class TollFree {

	public static void main(String[] args) {

		/* 1. - Passanger Carfee=2.50;2. - Busfee=5.00;3. - Truckfee=6.50; */
		
		
		int carType = 2;
		double fee;
		switch (carType) {
		case 1:
			fee = 2.50;
			System.out.println("Passenger car. Fee amount: " + fee);
			break;
		case 2:
			fee = 5.00;
			System.out.println("Bus. Fee amount: " + fee);
			break;
		case 3:
			fee = 6.50;
			System.out.println("Truck. Fee amount: " + fee);
			break;
		default:
			System.out.println("You can go for free.");
		}

	}

}
