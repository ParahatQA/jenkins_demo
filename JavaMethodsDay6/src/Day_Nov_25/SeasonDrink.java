package Day_Nov_25;

import java.util.Scanner;

public class SeasonDrink {
	
	public static void main(String[] args) {
		
		System.out.println("Enter the month");

		Scanner scan= new Scanner(System.in);
		
		int month=scan.nextInt();
		
		if(month==12 || month<=2) {
			System.out.println("Season: Winter");
			System.out.println("Drink: Pumpkin Spice");
			
		}else if (month<=5 ) {
			System.out.println("Season: Spring");
			System.out.println("Drink: Caramel Macchiato");
			
		}else if (month >=6 && month<=8) {
			System.out.println("Season: Summer");
			System.out.println("Drink: Iced Coffe");
			
		}else if (month==9||month==10 || month==11) {
			System.out.println("Season: Fall");
			System.out.println("Drink: Salted Caramel Mocha ");
			
		}else {
			System.out.println("Invalid number  for month");
		}
	}
	

}
