package Day_Nov_25;

import java.util.Scanner;

public class CarSalon {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.println("What kind of car would you prefer?");
		String choice = input.nextLine();

		System.out.println("What is your budget?");
		double amount = input.nextDouble();

		if (choice.equalsIgnoreCase("German")) {
			System.out.println("you selected German car");
			if (amount >= 3000) {
				System.out.println("we have a car for you");
			} else {
				System.out.println("we dont have a car that fits your budget");
			}

		} else if (choice.equalsIgnoreCase("American")) {
			System.out.println("You selected American car");
			if (amount >= 5000) {
				System.out.println("We have nice car for you");
			} else
				System.out.println("we dont have a car that fits your budget");

		} else if (choice.equalsIgnoreCase("Japanese")) {
			System.out.println("You selected Japanese car");
			if (amount >= 2000) {
				System.out.println("We have nice car for you");
			} else
				System.out.println("we dont have a car that fits your budget");
		}else {
			System.out.println("SORRY WE DONT HAVE THAT MODEL");
		}
	}
}
