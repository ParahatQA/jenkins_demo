package customclasses;

public class CustomClass2 {
	public static void main(String[] args) {
		
		
		CustomClass1 s1 = new CustomClass1();
		CustomClass1 s2 = new CustomClass1();

		s1.age = 5;
		s1.size = 36;
		s1.name = "Sarah";
		s1.length = 25.3;
		s1.location = "Chicago";

		System.out.println(s1.name + " is living in " + s1.location + " Illinois.");

		s1.shoeSize(20);
		s1.fruit("coconat", "orange");

		s2.age = 20;
		s2.size = 40;
		s2.name = "Mark";
		s2.length = 10.2;
		s2.location = "Seattle";

		System.out.println("-------------------------------");
		System.out.println(s2.name + " is " + s2.age + " years old " + " he lives in " + s2.location);

		s2.shoeSize(10);
		s2.fruit("strawberry", "banana");
	}
}
