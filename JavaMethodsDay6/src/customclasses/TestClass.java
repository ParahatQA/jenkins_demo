package customclasses;

import java.util.Scanner;

public class TestClass {
	public static void main(String[] args) {
		testCustomClass d1 = new testCustomClass();
		d1.name = "Kuzya";
		d1.breed = "chuwauwa";
		d1.age = 5;
		d1.bark("woof woof");
		d1.run("fast");

		Scanner input = new Scanner(System.in);
		System.out.println("How old is your dog?");
		int Age1 = input.nextInt();

		System.out.println(d1.age + Age1);

	}
}
