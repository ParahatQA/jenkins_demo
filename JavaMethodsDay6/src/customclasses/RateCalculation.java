package customclasses;

public class RateCalculation {

	public double calculateInvestmentValue(int originalAmount, double rate, int year) {

		rate = rate / 100;
		double interestAmount = originalAmount * rate * year;
		double totalValue = interestAmount + originalAmount;

		return totalValue;

	}

}
