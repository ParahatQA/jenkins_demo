package customclasses;

public class Person {
	String name;
	int age;
	char gender;
	String job;

	// void means nothing is returned from this method. this method only performs
	// some steps but does not give anything back

	public void run(int miles) {
		System.out.println(name + " is running " + miles + " miles");
	}

	public void swim(int meters) {
		System.out.println(name + " is swimming "+meters+" meters");
	}

	public void bike() {
		System.out.println(name + " is biking");
	}

	public void say(String speech) {
		System.out.println(name + speech);
	}

}