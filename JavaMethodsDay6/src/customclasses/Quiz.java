package customclasses;

public class Quiz {
	public static void main(String[] args) {
		String address = "www.turingscraft.com";

		System.out.println("http://" + address);

		int octet1 = 11;
		int octet2 = 22;
		int octet3 = 33;
		int octet4 = 44;
		System.out.println(octet1 + "." + octet2 + "." + octet3 + "." + octet4);

		String carzy = new String("CRAZY!\n\\\t\\\\\\\\\\n. . . .\\ \\\r\007'\\'\"TOOMUCH!");
		System.out.println(carzy);
		int cra = carzy.length();

		System.out.println(cra);

		int off = ( /* 5 + 3 */ -9 + 6/* 8 */ / 2); // *+4*/ -10);
		System.out.println(off);
		
		String mail = "myemail@gyber.com";
		
		mail = mail.toLowerCase();
		
		System.out.println(mail);
		
		String mail1 = mail.toUpperCase();
		
		System.out.println(mail1);
	}

}
