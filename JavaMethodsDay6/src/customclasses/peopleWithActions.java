package customclasses;

public class peopleWithActions {

	public static void main(String[] args) {
		Person p1 = new Person();
		p1.name = "Mike";
		p1.job = "java Developer";
		p1.age = 44;
		p1.gender = 'm';

		p1.run(3);
		p1.swim(5);
		p1.bike();
		p1.say(" Hello ");

		System.out.println("-------------------------");
		
		Person p2 = new Person();
		p2.name = "Shrek";
		p2.job = "C++ Developer";
		p2.age = 40;
		p2.gender = 'm';

		p2.run(10);
		p2.swim(3);
		p2.say("Hi");
		p2.bike();
	}
}
