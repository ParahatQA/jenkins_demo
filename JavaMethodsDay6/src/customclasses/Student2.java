package customclasses;

public class Student2 {

	public static void main(String[] args) {

		Student stu1 = new Student();
		Student stu2 = new Student();

		stu1.name = "Sasha";
		stu1.lastName = "Smith";
		stu1.schoolName = "SAC";
		stu1.age = 7;
		stu1.grade = 2;

	
		System.out.println("Student's name is " + stu1.name);
		System.out.println(stu1.name+" is "+stu1.age+" years old boy");
	
		
		System.out.println();
		//String name1 = stu1.getName("Chicago", "IL");
	
		
		
		stu2.name = "Maria";
		stu2.lastName = "Lopez";
		stu2.schoolName = "Frinedship";
		stu2.age = 15;
		stu2.grade = 9;

	}

}
