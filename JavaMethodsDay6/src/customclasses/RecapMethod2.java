package customclasses;

import java.util.Scanner;

public class RecapMethod2 {

	public static void main(String[] args) {
		Scanner r1 = new Scanner(System.in);

		System.out.println("Enter the car model: ");
		String carModel = r1.nextLine();

		System.out.println("Enter the car color:");
		String carColor1 = r1.next();

		System.out.println("Enter the cars speed: ");
		int carSpeed = r1.nextInt();

		RecapMethod Car=new RecapMethod();
		
		Car.model=carModel;
		Car.currentSpeed=carSpeed;
		Car.color=carColor1;
		
		Car.printCarInformation();
		Car.accelerate(30);
		Car.getCurrentSpeed();
		
	}

}
