package customclasses;

public class AirCon {
	
	String model;
	int temperature;
	boolean str;
	
	public void turnOn() {
		System.out.println("turning on " + model);
		str = true;
		
		
	}
	public void turnOff() {
		System.out.println("turning off " + model);
		str = false;
	}
	
	
	public void setTemperature(int temp) {
		temperature = temp;
	}
	
	public void turnUpTemperature(int addedValue) {

		temperature = temperature + addedValue;

	}
}
