package customclasses;

public class RecapMethod {

	String model;
	int currentSpeed;
	String color;

	public void printCarInformation() {
		System.out.println(color + " " + model + " is driving " + currentSpeed + " mph.");

	}

	public void accelerate(int moreSpeed) {
		currentSpeed = currentSpeed + moreSpeed;
		System.out.println("Cars speed after acceleration "+currentSpeed);
	}

	public int getCurrentSpeed() {
		return currentSpeed;

	}
}
