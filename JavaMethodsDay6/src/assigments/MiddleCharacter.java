package assigments;

import java.util.Scanner;

public class MiddleCharacter {

	public static void main(String[] args) {

		MiddleCharacter middleChar = new MiddleCharacter();

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the word");

		String str = scan.nextLine();

		System.out.println("The middle character in the string: " + middleChar.midChar(str));

	}

	public String midChar(String word) {
		int position;
		int length;

		if (word.length() % 2 == 0) {
			position = word.length() / 2 - 1;
			length = 2;

		} else {
			position = word.length() / 2;
			length = 1;
		}

		return word.substring(position, position + length);

	}
}
