package assigments;

import java.util.Scanner;

public class GreatestNumber {
	
	public static void main(String[] args) {

		GreatestNumber number = new GreatestNumber();		

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter first Number");
		int num1 = scan.nextInt();

		System.out.println("Enter second Number");
		int num2 = scan.nextInt();

		System.out.println("Enter third Number");
		int num3 = scan.nextInt();

		int largestNumber = number.getGreatest(num1, num2, num3);

		System.out.println("Greatest number is: " + largestNumber);

	}

	public int getGreatest(int firstNumber, int secondNumber, int thirdNumber) {

		if (firstNumber > secondNumber && firstNumber > thirdNumber) {

			return firstNumber;

		} else if (secondNumber > thirdNumber) {

			return secondNumber;
		} else {

			return thirdNumber;
		}
	}

}