package assigments;

import java.util.Scanner;

public class Test {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter the number of a week");
		
		int numDays = keyboard.nextInt();

		switch (numDays) {

		case 1:
			System.out.println("Day of the week is Monday\n It is a work day");
			break;
		case 2:
			System.out.println("Day of the week is Tuesday.\n It is a work day");
			break;
		case 3:
			System.out.println("Day of the week is Wednesday.\n It is a work day");
			break;
		case 4:
			System.out.println("Day of the week is Thursday.\n It is a work day");
			break;
		case 5:
			System.out.println("Day of the week  is Friday.\n It is a work day");
			break;
		case 6:
			System.out.println("Day of the week is Saturday.\n It is a weekend");
			break;
		case 7:
			System.out.println("Day of the week is Sunday.\n It is a weekend");
			break;

		default:
			System.out.println("Not a valid number of week. Please enter number from 1-7");
		}
	}

}
