package assigments;

import java.util.Scanner;

public class EvenOddNumber {

	public static void main(String[] args) {

		System.out.println("Enter the number");
		Scanner input = new Scanner(System.in);
		int number = input.nextInt();

		EvenOddNumber nums = new EvenOddNumber();

		System.out.println("The Number is  even:  " + nums.isEven(number));
		System.out.println("The Number is  odd:  " + nums.isOdd(number));

	}

		public boolean isEven(int num1) {
			if (num1 % 2 == 0) {
				return true;
			}
			return false;
		}
	
		public boolean isOdd(int num2) {
			if (num2 % 2 == 0) {
				return false;
			}
	
			return true;
		}

}
