package assigments;

import java.util.Scanner;

public class Alphabet {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Input an alphabet: ");
		String str = input.next().toLowerCase();

		boolean uppercase = str.charAt(0) >= 65 && str.charAt(0) <= 90;
		boolean lowercase = str.charAt(0) >= 97 && str.charAt(0) <= 122;
		boolean vowels = str.equals("a") || str.equals("e") || str.equals("i") || str.equals("o") || str.equals("u");

		if (str.length() > 1) {
			System.out.println("Error, not  single character");
		} else if (!(uppercase || lowercase)) {
			System.out.println("Error, Not  a letter. Enter upppercase or  lowercase letter");
		}

		else if (vowels) {
			System.out.println("Input letter is  vowel");
		} else {
			System.out.println("Input letter is consonant");
		}
	}

}
