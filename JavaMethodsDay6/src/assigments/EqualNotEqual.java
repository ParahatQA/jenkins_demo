package assigments;

import java.util.Scanner;

public class EqualNotEqual {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.println("Enter  the first  number");
		int number1 = input.nextInt();

		System.out.println("Enter  the second number");
		int number2 = input.nextInt();

		EqualNotEqual name = new EqualNotEqual();

		System.out.println(
				"Number " + number1 + " and number " + number2 + " are equal: " + name.isEqual(number1, number2));
		System.out.println("Number " + number1 + " and number " + number2 + " are not equal: "
				+ name.isNotEqual(number1, number2));

	}

	public boolean isEqual(int num1, int num2) {
		if (num1 == num2) {
			return true;
		}
		return false;
	}

	public boolean isNotEqual(int numA, int numB) {
		if (numA == numB) {
			return false;
		}
		return true;
	}

}
