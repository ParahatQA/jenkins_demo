package assigments;

import java.util.Scanner;

public class TernaryOperatorDemo {
	
	public static void main(String[] args) {
		
		Scanner scan= new  Scanner(System.in);
		
		
		System.out.println("Enter the  value of number 1: ");
		int  number1=scan.nextInt();
		
		System.out.println("Enter the  value of number 2: ");
		
		int number2= scan.nextInt();
		
		
		int min;
		int max;
		
		min=(number1<number2) ? number1 : number2;
		max=(number1<number2) ? number2 : number1;
		
		System.out.println("The minimum of two numbers: "+number1+" and "+number2+" is: "+min);
		System.out.println("The maximum of two numbers: "+number2+" and "+number1+" is: "+max);
	}
	
	
	
	
	
	
	

}
