package Day_10;

import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		char grade;
		
		System.out.println("Enter exam result: ");
		
		int examResult = input.nextInt();
		
		if (examResult > 90) {
			grade='A';
			System.out.println("make grade = 'A'");
			
		} else if (examResult > 80) {
			grade='B';
			System.out.println("make grade = 'B'");
			
		} else if (examResult > 70) {
			grade='C';
			System.out.println("make grade = 'C'");
			
		} else if (examResult > 65) {
			grade='D';
			System.out.println("make grade = 'D'");
		} else {
			System.out.println("You have failed");
		}
		
		//System.out.println("The Value of grades is "+grade);
	}
}
