package loops;

import java.util.Scanner;

public class GradeWithSwitch {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("enter the grade");
		char grade = scan.next().charAt(0);

		switch (grade) {
		case 'A':

			System.out.println("Excellent");
			break;

		case 'B':

			System.out.println("Good Job");
			break;

		case 'C':

			System.out.println("Good");
			break;

		case 'D':
			System.out.println("Poor");
			break;

		case 'F':

			System.out.println("Fail");
			break;

		default:
			System.out.println("Invalid grade.ABCDF only");

		}
	}

}
