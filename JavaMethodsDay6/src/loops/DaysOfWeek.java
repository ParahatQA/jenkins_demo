package loops;

public class DaysOfWeek {

	public static void main(String[] args) {
		
		String[] dayOfWeek = new String[7];
		
	
		dayOfWeek[0] = "Monday";
		dayOfWeek[1] = "Tuesday";
		dayOfWeek[2] = "Wednesday";
		dayOfWeek[3] = "Thursday";
		dayOfWeek[4] = "Friday";
		dayOfWeek[5] = "Saturday";
		dayOfWeek[6] = "Sunday";

		System.out.println("Days of  Week: ");

		for (String string : dayOfWeek) {
			System.out.println(string);
		}
		
		
//		for (int i = 0; i < dayOfWeek.length; i++) {
//			
//						
//			if (i==3) {
//				continue;
//			}
//			if (dayOfWeek[i].equals("Saturday")) {
//				break;
//			}
//
//			System.out.println(dayOfWeek[i]);
//		}

	}

}
