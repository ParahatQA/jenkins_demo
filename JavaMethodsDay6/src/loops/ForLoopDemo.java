package loops;

public class ForLoopDemo {
	
	public static void main(String[] args) {
		
		
		for (int i = 1; i < 11; i++) {
			System.out.print(i+" ");
		}
		
		//==========================================
		System.out.println();
		
		for (int i = 10; i >=1 ; i--) {
			System.out.print(i+" ");
		}
		
		//======================================================
		System.out.println();
		for (int i = 1; i < 11; i+=3) {
			System.out.print(i+" ");
		}
		
	}

}
