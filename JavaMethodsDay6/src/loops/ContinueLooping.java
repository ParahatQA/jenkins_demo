package loops;

public class ContinueLooping {

	public static void main(String[] args) {

//		for (int i = 0; i < 10; i++) {
//
//			if (i % 2 != 0) {
//				continue;
//			}
//
//			System.out.println("Even  Number " + i);
//		}
//		
		
		
		
		//==================================================
		//  loop  from  10 to 1 and print
		
		for (int i = 10; i >=1; i--) {

			if (i % 3!=0) {
				continue;
			}
			if(i%4==0) {
				break;
			}

			System.out.println("Even  Number " + i);
		}
		
	}

}
