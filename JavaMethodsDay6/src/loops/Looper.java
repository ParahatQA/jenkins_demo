package loops;

import java.util.Scanner;

public class Looper {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Enter  starting  number");
		int num1 = scan.nextInt();

		System.out.println("Enter  ending  number");
		int num2 = scan.nextInt();

		Looper looper = new Looper();
		
		
		looper.loopFromNumToNum(num1, num2);
	
		
		int sumOfNumbers = looper.loopAndSum(num1, num2);
		System.out.println("sum of the numbers " + sumOfNumbers);
	
	
		
	}

	 public void loopFromNumToNum(int start, int end) {
	
	 for (int i = start; i <= end; i++) {
	 System.out.print(i + " ");
	 }
	 System.out.println();

	
	 }

	//================================================
	 
	public int loopAndSum(int start, int end) {
		// return sum of all the numbers
		int total = 0;
		
		for (int i = start; i <= end; i++) {

			total = total + i;
		}

		return total;

	}

}
