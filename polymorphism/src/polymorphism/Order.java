package polymorphism;

public class Order {
	
	private String itemName;
	private int quantity;
	
	public Order(String itemName,int quantity){
		this.itemName=itemName;
		this.quantity=quantity;
		System.out.println("order constructor");
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	

}
