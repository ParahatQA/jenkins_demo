package polymorphism;

public class InStoreOrder extends Order {

	public InStoreOrder(String itemName, int quantity) {
		super(itemName, quantity);
		System.out.println("instore constructor");
		
	}

}
