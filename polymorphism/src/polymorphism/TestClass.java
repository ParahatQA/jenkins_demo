package polymorphism;

public class TestClass {
	public static void main(String[] args) {
		
		A a= new B();
		a.m1();
		
		//((B)a).m2();
		//OR
        B b=(B)a; 
		b.m2();
		b.m3();
 		
//		B b =(B)a;
//		b.m1();
//		b.m2();
		
		 
		
//		A a = new  C();
//		a.m1();
		
		
	
		
		
//		
//		A a = new C();
//		a.m1();
//		
//		a=new B();
//		a.m1();
//		A a = new D();
//		a.m1();
////		
//		a=(B)a;
		
	}

}
