package polymorphism;

import java.util.ArrayList;
import java.util.List;

public class OrdersTest {
	public static void main(String[] args) {
		
		Order order1= new Order("USB thumb drive", 5);
		System.out.println(order1.getItemName());
		
		OnlineOrder onlineorder1= new  OnlineOrder("Java book", 1);
		System.out.println(onlineorder1.getItemName());
		
		Order order2 = new OnlineOrder("Laptop",2);
		System.out.println(order2.getQuantity());

		
		Order order=new OnlineOrder("Iphone",1);
		System.out.println(order.getItemName());		 //IPhone
		
		 Order[] orders=new Order[3];
		 orders[0]=new OnlineOrder("Java book",1);
		 orders[1]=new InStoreOrder("USB charger",2);
		 orders[2]=new OnlineOrder("Printer",3);
		
		 String itemName=orders[2].getItemName();
		 System.out.println(itemName); 					//Printer

//		 int quantity=orders[2].getQuantity();
//		 System.out.println(quantity); 
//		
//		 List<Order>orderList= new ArrayList<>();
//		 orderList.add(onlineorder1);
//		 orderList.add(order1);
//		 orderList.add(new OnlineOrder("mouse",10));
		 
	}
}
