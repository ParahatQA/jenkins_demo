package polymorphism;

public class B extends A {
	

	public  void m1(){
		System.out.println("M1 FROM B");
	}

	
	public void m2(){
		System.out.println("M2 from B");
	}
	
	public void m3(){
		System.out.println("M3 from B");
	}
}
