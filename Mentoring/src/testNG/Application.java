package testNG;

import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class Application {

	SoftAssert softAssert;
	@BeforeSuite
	public void beforeSuite() {
		System.out.println("Before Suite");
	}

	@AfterSuite
	public void afterSuite() {
		System.out.println("After Suite");
	}

	@BeforeTest
	public void beforeTest() {
		System.out.println("Before executing test case");
	}

	@AfterTest
	public void afterTest() {
		System.out.println("Aftercuting test case");
	}

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("---Before method----");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("---After method---");
	}

	@Test(priority = 1)
	public void loginTest() {

		//throw new SkipException("Any Reason");
		softAssert=new SoftAssert();
		softAssert.fail("Error message");
		System.out.println("Login Test Execution");
		softAssert.assertAll();
	}

	@Test(priority = 2, dependsOnMethods={"loginTest"})
	public void passwordChangeTest() {
		
		softAssert=new SoftAssert();
		System.out.println("Changing  password");
		softAssert.assertEquals("A", "A");
		softAssert.assertAll();
	}

	@Test(priority = 3,dependsOnMethods={"loginTest"})
	public void logoutTest() {
		System.out.println("Logging out");
	}

}
