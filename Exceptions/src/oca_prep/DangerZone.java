package oca_prep;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class DangerZone {

	public static void main(String[] args) {

		
		DangerZone d= new  DangerZone();
				
		System.out.println("Start    ");
		
		try {
			d.dangerFileMethod();
		} catch (FileNotFoundException e) {
		
			System.out.println("Exception  caught  from File method");
		}

//		try {
//			dangerMethod();
//		} catch (Exception e) {
//			System.out.println("Dange caught");
//		}

		System.out.println("End");

	}

	 void dangerMethod() throws ArithmeticException {
		// we can use try catch to handle exception
		System.out.println(5 / 0);
		System.out.println("End of method call");
	}

	
	 void dangerFileMethod() throws FileNotFoundException {
		FileReader fr= new FileReader("SomeTextFile");
		
	}
}
