package oca_prep;

import java.io.FileNotFoundException;

public class SubDangerZone extends DangerZone{
	
	
	@Override
	void dangerMethod() throws RuntimeException {
		//  we  can  not  throw new  checked  exception
		// we  can throw  new  runtime exception
	}

	
	
	@Override
	void dangerFileMethod() throws FileNotFoundException {
		// when  overriding  method  throw  checked  exception  you  can  throw same  exception  method  declaration  or any subclass  of  that  exception
		//  when  you  override  you  can not  throw  new  or  broader  checked   exception  in  methdo  declaration
		
	}
}
