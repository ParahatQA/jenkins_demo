package Exception_Practice;

public class Test1 {

	public static void main(String[] args) {

		Test1 test = new Test1();
		test.printArrValues(args);
	}

	void printArrValues(String[] arr) {

		try {
			System.out.println(arr[0] + ":" + arr[1]);

		} catch (NullPointerException e) {
			System.out.println("NullPointerException");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("ArrayIndexOutOfBoundsException");
		} catch (IndexOutOfBoundsException e) {
			System.out.println("IndexOutOfBoundsException");
		}
	}

}
