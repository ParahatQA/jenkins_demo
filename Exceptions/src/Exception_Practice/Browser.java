package Exception_Practice;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;

public class Browser {

	public static void main(String[] args) throws FileNotFoundException, IOException {

		System.out.println(LocalDateTime.now());

		try {
			Browser.sleep(2);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

		System.out.println(LocalDateTime.now());

		readFile("file.txt");

		readBook("Selenium CookBook");

	}

	public static void sleep(int seconds) throws InterruptedException {
		Thread.sleep(seconds * 1000);
		// try {
		// Thread.sleep(seconds * 1000);
		// } catch (InterruptedException e) {
		// System.out.println(e.getMessage());
		// }
	}

	public static void readFile(String fileName) throws FileNotFoundException, IOException {
		FileReader reader = new FileReader("names.txt");
		reader.read();
	}

	public static void readBook(String title) throws IllegalArgumentException{

		System.out.println("Reading  a book : "+title);
	}

}
