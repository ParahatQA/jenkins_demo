package Exception_Practice;

import java.util.ArrayList;

public class ThrowDemo {

	public static void main(String[] args) {
		
	

		vote(new Citizen("Bob", 46));

		try {
			vote(new Citizen("Bob", 17));
		} catch (RuntimeException e) {
			System.out.println(e.getMessage());
		}
	}

	
	
	public static void vote(Citizen citizen) {
		if (citizen.getAge() < 18) {
			throw new ArithmeticException(citizen.getName() + " is not  eligible for voting");

		}

		System.out.println(citizen.getName() + " is eligible for voting");
	}

}
