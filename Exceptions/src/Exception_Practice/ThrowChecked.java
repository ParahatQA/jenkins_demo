package Exception_Practice;

import java.io.IOException;

public class ThrowChecked {
	
	public static void main(String[] args) {
		
		//  WE  NEED  TO HANDLE  CHECKED  EXCEPTION
		try {
			System.out.println("About to throw checked exception");
			throw  new IOException();
		} catch (Exception e) {
			
			System.out.println("Exception was  caught and handled");
			e.printStackTrace();
		}
		
	}
	

}
