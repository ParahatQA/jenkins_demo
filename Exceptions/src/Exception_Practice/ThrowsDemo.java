package Exception_Practice;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class ThrowsDemo {
	
	public static void main(String[] args) throws FileNotFoundException {
		
		
//		try {
//			FileReader reader= new FileReader("names.txt");
//		} catch (FileNotFoundException e) {
//			
//			e.printStackTrace();
//		}
		
		
		FileReader reader= new FileReader("names.txt");
	}

}
