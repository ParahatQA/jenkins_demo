package Exception_Practice;

import java.util.Scanner;

public class Login {

	public static final String USERID = "googleUser";
	public static final String PASSWORD = "java1234";

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("enter Used Id:");

		String userID = scan.next();

		System.out.println("enter password:");
		String pass = scan.next();

		if (!userID.equals(USERID)) {
			throw new RuntimeException("user id not found:" + userID);

		}
		if (!pass.equals(PASSWORD)) {
			throw new RuntimeException("user password is not found:" + pass);
		}
		System.out.println("Welcome " + userID);
	}

}
