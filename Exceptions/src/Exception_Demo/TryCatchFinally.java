package Exception_Demo;

public class TryCatchFinally {

	public static void main(String[] args) throws InterruptedException {

		try {
			int[] nums = new int[2];

			nums[0] = 5;
			nums[1] = 5;
			nums[2] = 5;

		} catch (ArrayIndexOutOfBoundsException e) {

			System.out.println("ArrayIndexOutOfBoundsException");

		} catch (Exception x) {
			
			System.out.println("Exception");
			
		} finally {
			
			System.out.println("Finally");
		}

	}

}
