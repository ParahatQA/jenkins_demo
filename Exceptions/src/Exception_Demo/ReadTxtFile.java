package Exception_Demo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

public class ReadTxtFile {
	
	public static void main(String[] args) {
		
		String filepath="C:\\Users\\Home\\Desktop\\not.doc";
		
		try {
			FileReader fs= new FileReader(filepath);
			BufferedReader reader= new BufferedReader(fs);
			String line=reader.readLine();
			System.out.println(line);
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		
		try {
			URL url= new URL("https://hello.com");
		} catch (MalformedURLException e) {
			System.out.println("Wrong  URL");
			e.printStackTrace();
		}
		
	}

}
