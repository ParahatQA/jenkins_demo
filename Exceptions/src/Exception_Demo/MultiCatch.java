package Exception_Demo;

public class MultiCatch {

	public static void main(String[] args) {

		String st = "Hello";
		try {
			int i = Integer.parseInt("12aws34");
			int l = st.length();
			System.out.println(i);
			System.out.println(l);

		} catch (NullPointerException e) {

			System.out.println("NullPointerException");
			
//		} catch (NumberFormatException e) {
//			System.out.println("NumberFormatException");
//		}
			
		} catch (RuntimeException e) {
			System.out.println("NumberFormatException");
		}
		
		
		System.out.println("After try  catch");
	}
}
