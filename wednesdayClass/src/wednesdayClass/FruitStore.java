package wednesdayClass;

public class FruitStore {
	
	public void sellFruit(Fruit fruit){
		
		if(fruit instanceof Apple){
			Apple app=(Apple)fruit;
			app.crushApple();
		}else if(fruit instanceof Orange){
			Orange org= (Orange)fruit;
			org.crushOrange();
		}else{
			System.out.println("i can  only  work with apple and orange");
		}
			
	}
	
	public static Edible getAnyEdible() {

		Edible e = new Apple();
		return e;
	}

	Fruit f2 = new Apple();
	Fruit f4 = new Orange();

	Edible edible=getAnyEdible();
	
	
	public static void main(String[] args) {
		
	
		Apple fruit1 = new Apple();
		// upcasting
		Fruit fruit2 = (Fruit)new Apple();
		Edible fruit3 = new Apple();
		
		//downcasting
		Fruit orange = new Orange();
	//	Apple fruit4 = (Apple) new Fruit();
		//======================================
		Apple f3 = new Apple();
		Fruit f2 = new  Apple();
		Edible f1=  new Apple();
		
		Fruit  f4= new Orange();
		
		Apple f5= (Apple)f2;				// ((Apple)f2).crushApple();
		Orange f6 = (Orange)f4;
		f5.crushApple();
		f6.crushOrange();
		
		
//		f1.eatIt();
//		
//		f2.eatIt();
//		f3.sliceFruit();
//		
//		f3.eatIt();
//		f3.sliceFruit();
//		
//		f4.eatIt();
//		f4.sliceFruit();
			
		
		//Apple fruit5 = (Apple) orange;
		
		// WebDriver driver = new ChromeDriver();
		
		
		// Apple a = (Apple) new Fruit();
		// instanceOf is the keyword for checking type compatibility
		
		Fruit f = new Apple();
		
		//System.out.println(fruit2 instanceOf Apple);
		
		boolean isApple = f instanceof Apple;
		boolean isOrange = f instanceof Orange;
		boolean isEdible = f instanceof Edible;
		boolean isFruit = f instanceof Fruit;
		
		Fruit ff = new Fruit();
		
		
		if(ff instanceof Apple){
			System.out.println("cast  it and  do apple specific stuff");
			Apple app =(Apple)ff;
			app.crushApple();
		}else{
			System.out.println("dont  cast JVM will explode");
		}

				
	}

}
