package webtests;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PopUpDemo {

	// There  are  2  types of  pop  ups
	// 1. browser  based  --> can be  handed by  selenium
	// also known as java script  alert  or  simply alert
	
	//2. os based pop  up  --> can not be  handed by selenium
	WebDriver driver;
	
	@ Before
	public void setUp(){
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("http://the-internet.herokuapp.com/javascript_alerts");
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		
		
	}
	//@Test
	public  void acceptPopUp(){
		driver.findElement(By.tagName("button")).click();
		//take  control of  the  popup
		//  this  line   will  fail if no alerts  available on the page  at this  moment
		Alert alert=driver.switchTo().alert();
		alert.accept();
		// after handling alerts there  is  not need to  call switchto.defaultContent()
		assertTrue(driver.findElement(By.xpath("//*[.='You successfully clicked an alert']")).isDisplayed());
		
		
	}
	
	//@Test
	public  void cancelPopUp(){
		driver.findElement(By.xpath("(//button)[2]")).click();
		Alert alert = driver.switchTo().alert();
		// dismiss()= used to  cancel the  alert
		alert.dismiss();
	
		assertTrue(driver.findElement(By.xpath("//*[.='You clicked: Cancel']")).isDisplayed());
	}
	
	@Test
	public  void sendKeysToPopUp(){
		driver.findElement(By.cssSelector("button[onclick='jsPrompt()']")).click();
		Alert alert = driver.switchTo().alert();
		alert.sendKeys("Hello");
		alert.accept();
		assertTrue(driver.findElement(By.xpath("//*[.='You entered: Hello']"))
				.isDisplayed());
				
		driver.findElement(By.cssSelector("button[onclick='jsPrompt()']")).click();
		 alert = driver.switchTo().alert();
		 alert.sendKeys("Hello");
		 //getText return  the  text of  the alert message, not the  text entered
		 System.out.println(alert.getText());
		 alert.dismiss();
		 assertTrue(driver.findElement(By.xpath("//*[.='You entered: null']")).isDisplayed());
		 alert=driver.switchTo().alert();
		 alert=driver.switchTo().alert();
		 
		 
		 
		 
	}
	
}
