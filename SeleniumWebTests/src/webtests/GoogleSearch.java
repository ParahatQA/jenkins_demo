package webtests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleSearch {
	
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");
		
		
		WebDriver driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		
		
		String url="http://google.com";
		driver.get(url);
		
		
		//verify title of  the  browser: Expected title: Google
		String expectedTitle="Google";
		String actualTitle=driver.getTitle();
		
		if(expectedTitle.equals(actualTitle)){
			
			System.out.println("Step passed. Title is as  expected");
			
		}else{
			System.out.println("Step failed. Title is as  expected");
		}
		
		// verify current url of  the pagecontains: www.google.com
		
		String expectedUrl="www.google.com";
		String actualUrl=driver.getCurrentUrl();
		System.out.println(actualUrl);
		
		if(actualUrl.contains(expectedUrl)){
			
			System.out.println("Step passed. URL is as  expected");
		}else{
			
			System.out.println("Step failed. URL is as  expected");
		}
		
		
		
		System.out.println("---------START Page source code --------------");
		String source=driver.getPageSource();
		System.out.println(source.length());
		
		System.out.println(driver.getPageSource().substring(0, 11));
		System.out.println("---------END Page source code --------------");
	
		// navigate
		
		//driver.navigate().to(url);
		//navigate().forward();
		//navigate().back();
		//driver.navigate().refresh();
		
		// Entera value  search button
		
		String searchItem="Selenium WebDriver";
		
		driver.findElement(By.name("q")).sendKeys("Selenium Webdriver");
		
		driver.findElement(By.name("btnG")).click();
	
		
		String titleAfterSearch=driver.getTitle();
		
		if(driver.getTitle().startsWith(searchItem)){
			
			System.out.println("PASSED");
			System.out.println(driver.getTitle());
		}else{
			
			System.out.println("FAILED");
			System.out.println(driver.getTitle());
		}
		
		WebElement searchResults=driver.findElement(By.id("resultStats"));
		System.out.println(searchResults.getText());
		
		System.out.println(driver);
	}

}
