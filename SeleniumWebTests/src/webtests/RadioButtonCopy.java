package webtests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class RadioButtonCopy{
	
	WebDriver driver;

	
	@Before
	public void setUp(){
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");
	    
		driver = new ChromeDriver();
	    
		driver.get("https://www.hotwire.com/");
		
	}
	
	@Test
	
	public void verifyDefaultTest(){
		// verify hoetl is  checked
		WebElement hotels=driver.findElement(By.cssSelector("input[id*='hotelRadio']"));
		Assert.assertTrue(hotels.isSelected());
		
	}
	
	
	
	
	//@Test
	public void homePageTest(){
			
		
		// VERIFY TITLE  STARTS WITH "CHEAP"
		String actualTitle = driver.getTitle();
		String expected = "Cheap";
		Assert.assertTrue(actualTitle.startsWith(expected));
		
		//VERIFY URL IS "https://www.hotwire.com/"
		
		String actualUrll =  driver.getCurrentUrl();
		String expectedUrl="https://www.hotwire.com/";
		Assert.assertTrue(actualUrll.equals(expectedUrl));
	}	
		
	
	//	@Test
	public void test(){
		
		String expected ="one ";
		String actual="one";
		boolean equals=  expected.equals(actual);
		
		Assert.assertTrue(expected.equals(actual));
	}

	
	
}

