package webtests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class RadioButtonExample {

	WebDriver driver;
	WebElement hotels, cars, flights;

	@BeforeClass
	public static void beforeClass() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");
	}

	@Before

	public void setUp() {

		driver = new ChromeDriver();
		driver.get("https://www.hotwire.com/");

		hotels = driver.findElement(By.cssSelector("input[id*='hotelRadio']"));
		cars = driver.findElement(By.cssSelector("input[id*='carRadio']"));
		flights = driver.findElement(By.cssSelector("input[id*='flightRadio']"));
	}

	@After
	public void tearDown() {
		driver.close();
	}

	@Test

	public void flightRadioTest() {

		flights.click();

		Assert.assertFalse(hotels.isSelected());
		Assert.assertFalse(cars.isSelected());
		Assert.assertTrue(flights.isSelected());

		WebElement findAFlightBtn = driver.findElement(By.cssSelector("#ffAirRoundTripButton"));
		Assert.assertTrue(findAFlightBtn.isDisplayed());

	}

	@Test

	public void carRadioTest() {

		cars.click();

		Assert.assertFalse(hotels.isSelected());
		Assert.assertTrue(cars.isSelected());
		Assert.assertFalse(flights.isSelected());

		WebElement findACarBtn = driver.findElement(By.cssSelector("#findCarButton"));
		Assert.assertTrue(findACarBtn.isDisplayed());

	}

	@Test
	public void verifyDefaultsTest() {

		// WebElement hotels =
		// driver.findElement(By.cssSelector("input[id*='hotelRadio']")); //
		// verify Hotels checked
		Assert.assertTrue(hotels.isSelected());

		// WebElement cars =
		// driver.findElement(By.cssSelector("input[id*='hotelRadio']"));
		Assert.assertFalse(cars.isSelected()); // asertFalse expects False if
												// itb gets true it fails

		// WebElement flights =
		// driver.findElement(By.cssSelector("input[id*='hotelRadio']"));
		Assert.assertFalse(flights.isSelected());
		// <input id = "random232342-carRadio">

		WebElement roomsEl = driver.findElement(By.cssSelector("select#hotelsRooms"));
		Select rooms = new Select(roomsEl);
		String actuallySelected = rooms.getFirstSelectedOption().getAttribute("value");
		String expected = "1";

		// compared two arguments and fails if they are not equal
		Assert.assertEquals(expected, actuallySelected);

		WebElement findAHotelBtn = driver.findElement(By.cssSelector("#findHotelButton"));
		Assert.assertFalse(findAHotelBtn.isDisplayed());

		// css selector using contains
		// tag[attribute*='text']
		// input[id*='hotelRadio']
		// in css * means contains
		// $ means ends with
		// ^ means start with
		// # means ID

	}

	@Test
	public void homePageTest() {

		String actualTitle = driver.getTitle();
		String expected = "Cheap";
		Assert.assertTrue(actualTitle.startsWith(expected));

		String actualUrl = driver.getCurrentUrl();
		String expectedUrl = "https://www.hotwire.com/";

		Assert.assertTrue(actualUrl.equals(expectedUrl));

	}

	@Ignore
	@Test
	public void test() {

		String expected = "one";
		String actual = "one";

		// boolean equals=expected.equals(actual);

		Assert.assertTrue(expected.equals(actual)); // assert true expect true
													// as argument, fails
													// otherwise

		// Assert.assertTrue(2!=2); // assertion failure stops the execution of
		// that method.
		// Assert.assertTrue(2==2);

	}

}
