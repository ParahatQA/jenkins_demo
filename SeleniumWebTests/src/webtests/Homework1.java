package webtests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Homework1 {
	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		// WebDriver driver2= new FirefoxDriver();

		driver.get("http://newtours.demoaut.com/mercuryregister.php");

		// String firstName= "John";
		driver.findElement(By.xpath("(//input[@maxlength='60'])[1]")).sendKeys("John");

		driver.findElement(By.name("lastName")).sendKeys("Smith");

		// int phone= 123456789;
		driver.findElement(By.name("phone")).sendKeys(Integer.toString(123456789));

		driver.findElement(By.name("userName")).sendKeys("johnsmith@mail.com");

		driver.findElement(By.name("address1")).sendKeys("123 Any st");

		driver.findElement(By.name("city")).sendKeys("Chicago");
		driver.findElement(By.name("state")).sendKeys("Illinois");

		driver.findElement(By.name("postalCode")).sendKeys(Integer.toString(60056));

		driver.findElement(By.name("email")).sendKeys("johnsmith");
		driver.findElement(By.name("password")).sendKeys("test");
		driver.findElement(By.name("confirmPassword")).sendKeys("test");
		driver.findElement(By.name("register")).click();

		String url = driver.getCurrentUrl();
		System.out.println(url);

		String actual = "http://newtours.demoaut.com/create_account_success.php?osCsid=204eb95fdb385d09769aec39a79b04c7";

		if (actual.equals(url)) {
			System.out.println("pass");
		} else {
			System.out.println("fail");
		}

		driver.findElement(By.xpath("//a[@href='mercurywelcome.php']")).click();
		driver.findElement(By.name("userName")).sendKeys("johnsmith");
		driver.findElement(By.name("password")).sendKeys("test");
		driver.findElement(By.name("login")).click();

		String currentUrl = driver.getCurrentUrl();
		System.out.println(currentUrl);

		String actual1 = "http://newtours.demoaut.com/mercurysignon.php";

		System.out.println("fail");
		
		if (actual1.equals(currentUrl)) {
			System.out.println("pass");
		} else {
		}
			
			
	}

			public static void verification(String url, String actual){
					if(actual.equals(url)){
						System.out.println("pass");
					}else{
						System.out.println("fail");
					}}
			


}
