package webtests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.safari.SafariDriver;


public class FirstSeleniumTest {
	public static void main(String[] args) {
		
		//System.out.println(System.getProperty("os.name"));
		// property 
		// key  value (os name"windows 10" )
	  	
		//	driver.get("http://google.com");

			
	
		//		System.setProperty("webdriver.gecko.driver","C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");
		//		
		//		// opening a new driver
		//		WebDriver driver  = new FirefoxDriver();
		//		
		//		
		//		// goes to a  web page
		//		driver.get("http://amazon.com");
		
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");
		
		
		WebDriver driver  = new ChromeDriver();				// opening a new driver
		
		
		
		driver.get("http://amazon.com");  					// goes to a  web page
		
		
		//driver.navigate().to("http://amazon.com");  		 // goes to a  web page
		
		
		
		//driver.quit();
											// closes the driver and  makes the  driver equal to null
											// quit  closes  all  tabs
		 
		//driver.get("http://google.com"); 
											//driver.close();
											// closes  the  current tab only
											//only closes the  browser, driver object is  still there
		
		// driver.navigate().back();
		//		 
		// driver.navigate().forward();
		
		
//		String url = driver.getCurrentUrl();
//		System.out.println(url);
//		
		String actualTitle = driver.getTitle();
		//System.out.println(actualTitle);
		
		String expectedTitle ="Amazon.com: Online shopping for electronics, apparel, computers, books, dvds";
		
		if(actualTitle.equals(expectedTitle)){
			
			System.out.println("PASS");
			
		}else{
			System.out.println("FAIL");
			System.out.println("EXPECTED : "+expectedTitle);
			System.out.println("ACTUAL:  " +actualTitle );
		}
		
				
		String actualUrl = driver.getCurrentUrl();
		
		String expectedUrl =  "http:// amazon.com";
			
		if (actualUrl.equals(expectedUrl)) {

			System.out.println("PASS");

		} else {
			System.out.println("FAIL");
			System.out.println("EXPECTED : " + expectedUrl);
			System.out.println("ACTUAL:  " + actualUrl);
		}
		
		System.out.println(driver.getPageSource());		
		driver.close();
		
		
	

}}
