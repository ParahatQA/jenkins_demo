package webtests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.jar.Attributes.Name;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class EmployeesTable {
	
	WebDriver driver;
	
	@Before
	public  void  setup(){
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://editor.datatables.net/examples/simple/simple.html");
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		
	}
	
	
	//@Test
	
		public void verifyDefaults(){
//		Verify web table is displayed on page
		WebElement table=driver.findElement(By.id("example"));
		assertTrue(table.isDisplayed());
	
//		Verify number of columns
		List<WebElement>headers= driver.findElements(By.xpath("//table[@id='example']/thead//th"));
		assertEquals(6,headers.size());
		
//		Verify number of rows (without the headers and footers) 10
		List<WebElement> rows= driver.findElements(By.xpath("//table[@id='example']/tbody/tr"));
		assertEquals(10,rows.size());
		
//		Verify that every header is the same as the footer
		List<WebElement> footers= driver.findElements(By.xpath("//table[@id='example']/tfoot//th"));
		assertEquals(headers.size(), footers.size());
		
		for (int i = 0; i < headers.size(); i++) {
			assertEquals(headers.get(i).getText(), footers.get(i).getText());
			
		}
		
//		Verify the top name entry on the table Airi Satou
		assertEquals("Airi Satou", driver.findElement(By.xpath("//table[@id='example']/tbody/tr[1]/td[1]")).getText());
		
//		Verify name entry exists on table Bruno Nash
		List<WebElement> name= driver.findElements(By.xpath("//table[@id='example']/tbody/tr/td[1]"));
		List<String> actualName= new  ArrayList<>();
		// get  the  string  names and  save it  in the new  string list
		for (WebElement nameElement : name) {
			actualName.add(nameElement.getText());
		}
		assertTrue(actualName.contains("Bruno Nash"));
		// verify  the  position  entry  for  bruno  nash software  engineer
		// find  bruno
		// the  row
		// find  position
		WebElement  position= driver.findElement(By.xpath("//table[@id='example']//td[.='Bruno Nash']/../td[2]"));
		assertEquals("Software Engineer", position.getText());
		//verify the  office entry for bruno nash london 
		WebElement  office= driver.findElement(By.xpath("//table[@id='example']//td[.='Bruno Nash']/../td[3]"));
		assertEquals("London", office.getText());
		
//		Verify the Extension entry  for Bruno Nash 6222
		WebElement  extension= driver.findElement(By.xpath("//table[@id='example']//td[.='Bruno Nash']/../td[4]"));
		assertEquals("6222", extension.getText());
	
//		Verify the Start date entry for Bruno Nash
		WebElement  date= driver.findElement(By.xpath("//table[@id='example']//td[.='Bruno Nash']/../td[5]"));
		assertEquals("2011-05-03", date.getText());
				
//		Verify the Salary entry for Bruno Nash
		WebElement  salary= driver.findElement(By.xpath("//table[@id='example']//td[.='Bruno Nash']/../td[6]"));
		assertEquals("$163,500", salary.getText());
		
//		Verify that the entries on the Name column are sorted in ascending order
		
		// 1  have  the  original  list 
		// 2  sort  the  original  list  from   step  one a  new  list
		List<String> sortedName= new ArrayList<>();
		sortedName.addAll(actualName);
		Collections.sort(sortedName);		
		// compare  lists
		System.out.println("Original");
		System.out.println(actualName);
		System.out.println("sorted");
		System.out.println(sortedName);
		
		assertArrayEquals(sortedName.toArray(), actualName.toArray());  // assertArrayEquals compares  arrays
																		// toArray is a built method for lists  and  converts the list  to  an array
		
		
		// sorting in  reverse  order
		// Collections.sort(sortedNames, Colections.reverseOrder());
	}

	@Test
	public  void verifyingSorting(){
//		1	Open browser	
//		2	Go to url	https://editor.datatables.net/examples/simple/simple.html
//		3	Click on column name	Name
		WebElement  name= driver.findElement(By.xpath("//table[@id='example']/thead/tr/th[.='Name']"));
		name.click();

//		4	Verify the top name entry on the table	Zorita Serrano
		WebElement name1=driver.findElement(By.xpath("//tbody/tr/td[.='Zorita Serrano']"));
		assertEquals("Zorita Serrano", name1.getText());
		
//		5	Verify that the entries on the Name column are sorted in decending order	
		WebElement  nameDesc= driver.findElement(By.xpath("//table[@id='example']/thead/tr/th[.='Name']"));
		nameDesc.click();

		
//		6	Verify the top name entry on the table	Airi Satou
		WebElement nameAfterSorted=driver.findElement(By.xpath("//tbody/tr/td[.='Airi Satou']"));
		assertEquals("Airi Satou", nameAfterSorted.getText());
		
		
//		7	Verify name entry exists on table	Bruno Nash
		
		List<WebElement> name3= driver.findElements(By.xpath("//table[@id='example']/tbody/tr/td[1]"));
		
		List<String> actualName= new  ArrayList<>();
		// get  the  string  names and  save it  in the new  string list
		for (WebElement nameElement : name3) {
			actualName.add(nameElement.getText());
		}
		assertTrue(actualName.contains("Bruno Nash"));
		
		
//		8	Verify the Position entry for Bruno Nash	Software Engineer
		WebElement  pos= driver.findElement(By.xpath("//table[@id='example']//td[.='Bruno Nash']/../td[2]"));
		assertEquals("Software Engineer", pos.getText());
		
		
//		9	Verify the Office entry  for Bruno Nash	London
		WebElement  office= driver.findElement(By.xpath("//table[@id='example']//td[.='Bruno Nash']/../td[3]"));
		assertEquals("London", office.getText());
		
		
//		10	Verify the Extension entry  for Bruno Nash	6222
		
		WebElement  extension= driver.findElement(By.xpath("//table[@id='example']//td[.='Bruno Nash']/../td[4]"));
		assertEquals("6222", extension.getText());
		
//		11	Verify the Start date entry for Bruno Nash	40666
		WebElement  startDate= driver.findElement(By.xpath("//table[@id='example']//td[.='Bruno Nash']/../td[5]"));
		assertEquals("2011-05-03", startDate.getText());
		
		
//		12	Verify the Salary entry for Bruno Nash	163500
		WebElement  salary= driver.findElement(By.xpath("//table[@id='example']//td[.='Bruno Nash']/../td[6]"));
		assertEquals("$163,500", salary.getText());
		
		
//		13	Verify that the entries on the Name column are sorted in ascending order
		List<WebElement> sortList= driver.findElements(By.xpath("//table[@id='example']/tbody/tr/td[1]"));
		List<String> actualsortList= new  ArrayList<>();
		
		List<String> sortedName= new ArrayList<>();
		sortedName.addAll(actualsortList);
		//Collections.sort(sortedName);	
		Collections.sort(sortedName);
		
			
//		14	Click on column name	Name
		WebElement  colName= driver.findElement(By.xpath("//table[@id='example']/thead/tr/th[.='Name']"));
		colName.click();
		
		
//		15	Verify the top name entry on the table	Zorita Serrano
		
		WebElement nameZorito=driver.findElement(By.xpath("//tbody/tr/td[.='Zorita Serrano']"));
		assertEquals("Zorita Serrano", nameZorito.getText());
		
		
//		16	Click on column name	Salary
		WebElement  salName= driver.findElement(By.xpath("//table[@id='example']/thead/tr/th[.='Salary']"));
		salName.click();
		
//		17	Verify the top name entry on the table	Jennifer Acosta
		WebElement namejenni=driver.findElement(By.xpath("//tbody/tr/td[.='Jennifer Acosta']"));
		assertEquals("Jennifer Acosta", namejenni.getText());
		
//		18	Verify the last name entry on the table	Thor Walton
		WebElement nameThor=driver.findElement(By.xpath("//tbody/tr/td[.='Thor Walton']"));
		assertEquals("Thor Walton", nameThor.getText());
		
//		19	Verify that the entries on the Salary column are sorted in ascending order	
		
		List<WebElement> salary1= driver.findElements(By.xpath("//table[@id='example']/tbody/tr/td[6]"));
		List<String> salaryList= new  ArrayList<>();
				
		List<String>sortedSalary=new ArrayList<>();
		sortedSalary.addAll(salaryList);
		Collections.sort(sortedSalary);
		
				
//		20	Click on column name	Salary
		WebElement  salName1= driver.findElement(By.xpath("//table[@id='example']/thead/tr/th[.='Salary']"));
		salName1.click();
		
//		21	Verify the top name entry on the table	Angelica Ramos
		
		WebElement nameAngi=driver.findElement(By.xpath("//tbody/tr/td[.='Angelica Ramos']"));
		assertEquals("Angelica Ramos", nameAngi.getText());
		
//		22	Verify the last name entry on the table	Brielle Williamson
		
		WebElement nameBrielle=driver.findElement(By.xpath("//tbody/tr/td[.='Brielle Williamson']"));
		assertEquals("Brielle Williamson", nameBrielle.getText());
		
		
//		23	Verify that the entries on the Salary column are sorted in descending order	
		List<WebElement>salaryDescending=driver.findElements(By.xpath("//table[@id='example']/tbody/tr/td[6]"));
		List<String> descendingSal= new  ArrayList<>();
		
		List<String>sortedSal=new ArrayList<>();
		sortedSal.addAll(descendingSal);
		Collections.sort(sortedSal, Collections.reverseOrder());

	}
}
