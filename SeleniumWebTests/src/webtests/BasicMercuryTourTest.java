package webtests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BasicMercuryTourTest {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.get("http://newtours.demoaut.com/mercuryregister.php");

		String fName = "John";
		driver.findElement(By.name("firstName")).sendKeys(fName);

		String lastName = "Smith";
		driver.findElement(By.name("lastName")).sendKeys(lastName);

		int phone = 12341234;
		driver.findElement(By.name("phone")).sendKeys(Integer.toString(phone));
		// driver.findElement(By.name("phone")).sendKeys(phone+"");

		String email = "johnsmith@mail.com";
		driver.findElement(By.name("userName")).sendKeys(email);

		// Thread.sleep(5000);

		String actualFName = driver.findElement(By.name("firstName")).getAttribute("value");
		compareStrings(fName, actualFName);

		String actualLastName = driver.findElement(By.name("lastName")).getAttribute("value");
		compareStrings(lastName, actualLastName);

		String actualPhoneNumber = driver.findElement(By.name("phone")).getAttribute("value");
		compareStrings(Integer.toString(phone), actualPhoneNumber);

		String actualEmail = driver.findElement(By.name("email")).getAttribute("value");
		compareStrings(email, actualEmail);

		String userName = "johnsmith";
		driver.findElement(By.name("email")).sendKeys(userName);

		String password = "test";
		driver.findElement(By.name("password")).sendKeys(password);

		driver.findElement(By.name("confirmPassword")).sendKeys(password);

		driver.findElement(By.name("register")).click();

		String fullName = fName + " " + lastName;
		String message = driver.findElement(By.xpath("//b")).getText();

		int i1 = message.indexOf("");
		int i2 = message.indexOf("");

		String actualFullName = message.substring(i1 + 1, i2);
		System.out.println(message);
		compareStrings(fullName, actualFullName);

		String message2 = driver.findElement(By.xpath("(//b)[2]")).getText();
		System.out.println(message2);
		
		int i3= message2.indexOf(" ");
		int i4 = message.indexOf(".");
		
		String actualUserName=message2.substring(i3, i4);
		System.out.println(actualUserName);
		
		compareStrings(userName, actualUserName);
		

	}

	public static void compareStrings(String expected, String actual) {
		if (expected.equals(actual)) {
			System.out.println("pass");
		} else {
			System.out.println("fail");
			System.out.println("Expected: " + expected);
			System.out.println("Actual: " + actual);
		}

	}

}
