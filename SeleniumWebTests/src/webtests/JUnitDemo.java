package webtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.assertFalse;
import org.junit.Test;


public class JUnitDemo {

	@AfterClass

	public static void tearDownAfterClass() {
		System.out.println("runs  after  everything");
	}

	@BeforeClass
	public void setUpBeforeClass() {

		System.out.println("runs  before everything");
	}

	@Before
	public void tearDown() {
		System.out.println("before test");

	}

	@Test
	public void test() {
		System.out.println("test 1");
		assertTrue(true);
	}

	@Test

	public void assertsFalse() {
		System.out.println("test 2");
		assertFalse(false);
	}

	@Test
	public void assertEquality() {
		System.out.println("Test 3");
		assertEquals(1, 1);
	}

	public boolean alwaysFalse() {
		return false;

	}
}
