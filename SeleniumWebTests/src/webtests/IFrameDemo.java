package webtests;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class IFrameDemo {
	
	WebDriver driver;

	//@Test
	public void iFrameTest() {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");
		 driver = new ChromeDriver();
		driver.get("http://the-internet.herokuapp.com/iframe");
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

		assertTrue(driver.findElement(By.linkText("Elemental Selenium")).isDisplayed());
		// switch  to  iframe using a  webelement
		// switch  iframe
		
		WebElement iframe =driver.findElement(By.id("mce_0_ifr"));
		driver.switchTo().frame(iframe);
		driver.findElement(By.id("tinymce")).clear();
		// switch  back  to  main content
		driver.switchTo().defaultContent();
		assertTrue(driver.findElement(By.linkText("Elemental Selenium")).isDisplayed());
	}

	
	//@Test
	public void iframeTetsUsingID(){
		
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://the-internet.herokuapp.com/iframe");
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		assertTrue(driver.findElement(By.linkText("Elemental Selenium")).isDisplayed());
		// switch  to  iframe using a  webelement
		// switch  iframe
		
		
		driver.switchTo().frame("mce_0_ifr");
		driver.findElement(By.id("tinymce")).clear();
		// switch  back  to  main content
		driver.switchTo().defaultContent();
		assertTrue(driver.findElement(By.linkText("Elemental Selenium")).isDisplayed());
		
	}
	
	
	@Test
	public void iframeTetsUsingIndex(){
		
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://the-internet.herokuapp.com/iframe");
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		assertTrue(driver.findElement(By.linkText("Elemental Selenium")).isDisplayed());
		// switch  to  iframe using a  webelement
		// switch  iframe
		
		
		driver.switchTo().frame(1);
		driver.findElement(By.id("tinymce")).clear();
		// switch  back  to  main content
		driver.switchTo().defaultContent();
		assertTrue(driver.findElement(By.linkText("Elemental Selenium")).isDisplayed());
		
	}
	
	
	
	
	
}
