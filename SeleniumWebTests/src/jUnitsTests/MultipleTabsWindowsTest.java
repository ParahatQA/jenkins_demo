package jUnitsTests;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MultipleTabsWindowsTest {
	WebDriver driver;
	
	@Before
	public void setUp(){
		
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		
		driver.get("http://the-internet.herokuapp.com/windows");
	}
	
	@Test
	public  void multiTabTest(){
	
		System.out.println(driver.getTitle());
		// selenium does not change tabs  or  windows  automatically
		// we  need to  call the driver.switchto.window method
		driver.findElement(By.linkText("Click Here")).click();
		
		// getWindowHandle methods returns the  unique id of  the current window/tab selenium is  controlling
		String originalWindow= driver.getWindowHandle();
		System.out.println(originalWindow);
		//getWindowHandles() return the  list  of all window  handles all  tabs  all  windows
		
		List<String>handles = new ArrayList<> (driver.getWindowHandles());
		System.out.println(handles);
		//switching to the  second window handle  from the handles  list
		driver.switchTo().window(handles.get(1));
		
		System.out.println(driver.getTitle());
		
		
	}
	
	//@Test
	public  void changeWindowUsingMethod(){
		
		// write   method that  takes a  title we  want to switch to as  a  parametr and  switches to page with the title
		driver.findElement(By.linkText("Click Here")).click();
		System.out.println(driver.getTitle());
		switchWindow("New Window");
		System.out.println(driver.getTitle());
		
	}
	
	public  void switchWindow(String title){
		String original= driver.getWindowHandle();
		// get all available handles and iterate
		for (String handle : driver.getWindowHandles()) {
			// switch to handle
			driver.switchTo().window(handle);
			// get  title
			// verify if it is  the expected
			if(title.equals(driver.getTitle())){
				// break  the  loop and  get  out of  the  method
				return;
			}

			// if not go to the next handle

		}
		// swicth back to original tab
		// this  line  will  run only if  title passed as  parametr did  not match any  available window title
		driver.switchTo().window(original);
		
	}

	//@Test
	public  void switchToSeparateWindow(){
		driver.get("http://toolsqa.com/automation-practice-switch-windows/");
		System.out.println(driver.getTitle());
		driver.findElement(By.id("button1")).click();
		switchWindow("QA Automation Tools Tutorial");
		System.out.println(driver.getTitle());
	}
	
}
