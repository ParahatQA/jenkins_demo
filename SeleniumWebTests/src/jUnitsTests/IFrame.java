package jUnitsTests;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class IFrame {

	WebDriver driver;
	
	@Before
	public void setUp() throws Exception {
		
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");
		driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		//String url="file:///C:/Users/Parahat/Documents/iframes.html";
		//navigate to  google homepage
		
		driver.get("http://hotwire.com");
		driver.manage().window().maximize();
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		//Assert.assertTrue(driver.findElement(By.linkText("HotelLinkToVerify")).isDisplayed());
		List<WebElement> framesCollection=driver.findElements(By.tagName("iframe"));
		System.out.println("Number of  frames in the page: "+framesCollection.size());
		
		for(WebElement e: framesCollection){
			driver.switchTo().frame(e);
			List<WebElement>framesInsideFrames=driver.findElements(By.tagName("iframe"));
			System.out.println("Number of frames inside the frame is: "+framesInsideFrames);
			driver.switchTo().defaultContent();
		}
		
		
		
		
		// Switch to a  frame using index
//		driver.switchTo().frame(0);
//		Assert.assertTrue(driver.findElement(By.linkText("HOtels")).isDisplayed());
//		driver.switchTo().defaultContent();
		
		
		//Switch to a  frame  using  name 
//		driver.switchTo().frame("iframe2");
//		driver.findElement(By.linkText("Hotels")).click();
//		driver.switchTo().defaultContent();
		
		//Switch to  a  frame using ID
		
//		driver.switchTo().frame("iF1");
//		driver.findElement(By.linkText("Cars")).click();
//		driver.switchTo().defaultContent();

		// switch to frame using webelement frame
		WebElement iframeElement=driver.findElement(By.id("iF2"));
		driver.switchTo().frame(iframeElement);
		driver.findElement(By.linkText("Flights")).click();
		driver.switchTo().defaultContent();
		
		
		
		
	}

}
