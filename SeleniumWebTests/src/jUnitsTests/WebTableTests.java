package jUnitsTests;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTableTests {
	
	WebDriver driver;	
	WebElement table;
	
	
	@Before
	public  void setUp(){
	
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");
		driver= new ChromeDriver();
		driver.get("http://the-internet.herokuapp.com/tables");
		table=driver.findElement(By.id("table1"));
				
	}
	
	@After
	public void tearDown(){
	  driver.quit();
	}
	
	
	//@Test
	public void printTableText(){
		
		WebElement table=driver.findElement(By.id("table1"));
		//System.out.println(table.getText());
		
		// get  the  number  of  columns
		// goes  to the  first  row
		// then  get   all child elements of  the first  row
		List<WebElement> columnNames= driver.findElements(By.cssSelector("#table>thead th"));
		//count
		System.out.println("Number  of  column  "+columnNames.size());
		for(WebElement columnName: columnNames ){
			System.out.println(columnName.getText());
		}
			
		
		
		//only count the  rows except headers
		
//		List<WebElement> rows = driver.findElements(By.xpath("//table[@id='table1']/tbody/tr"));
//		System.out.println("Number  of  rows"+rows.size());
//		
//		for(WebElement row: rows){
//			System.out.println(row.getSize());
//		}
	}
		
		//@Test
		public void printByCoordinates(){
			
			int row=1, col=1;
			
			
			String xpath= getCellData(row,col);
			WebElement cell=driver.findElement(By.xpath(xpath));
			System.out.println(cell.getText());
			row=4;
			col=4;
			xpath=getCellData(row, col);
			cell= driver.findElement(By.xpath(xpath));
			System.out.println(cell.getText());
			
	}
		
		public String getCellData(int row, int col){
			return "//table[@id='table1']/tbody/tr["+row+"]/td["+col+"]";
		}
		
		
		
		@Test
		public void printCoordinates(){
			String string="Smith";
			
			// get  all  rows
			// get text  of  all  rows
			// see what  row  contains expected string
			// get  the index of that row
			
			List<WebElement> rows = driver.findElements(By.xpath("//table[@id='table1']/tbody/tr"));
			System.out.println(rows.size());
			
			int rowNumber=0;
			for (int i = 0; i < rows.size(); i++) {
				if(rows.get(i).getText().contains(string)){
					rowNumber=i+1;
					break;
				}

		}
			
			System.out.println(rowNumber);
			
			
			
			// get all columns for that  row
			String xpath = "//table[@id=''table1]/tbody/tr["+rowNumber+"]/td";
										//"//table[@id=''table1]/tbody/tr[3]/td";
			List<WebElement> cells= driver.findElements(By.xpath(xpath));
			int colNumber=0;
			
			for (int i = 0; i < cells.size(); i++) {
				if(cells.get(i).getText().equals(string)){
					colNumber=i+1;
					break;
			}

		}
			System.out.println(rowNumber);

	}
		//@Test
		public void printCoordinates2(){
			// find the  table
			table=driver.findElement(By.id("table1"));
			// get the  number  of  the  rows
			int totalRows=getTotalRows(table);
			System.out.println(totalRows);
			// get the  number of  the  columns
			int totalCols=getTotalCols(table);
			System.out.println(totalCols);
			// call  getCellDate() to  bouild the  xpath
			// do  it for  all  cells
			
			
		for (int i = 1; i <= totalRows; i++) {
			for (int j = 1; j <= totalCols; j++) {
				String xpath=getCellData(i,j);
				System.out.print(driver.findElement(By.xpath(xpath)).getText()+"\t");
			}
			System.out.println();
		}

	}
		private int getTotalCols(WebElement table2) {
			
			return table.findElements(By.tagName("th")).size();
		}

		private int getTotalRows(WebElement tabnle){
			// find out if  the  table  has  header
			if(table.findElements(By.tagName("thead")).size()>0){
				return table.findElements(By.tagName("tr")).size()-1;
			}else{
				return table.findElements(By.tagName("tr")).size();
		}

	}
		
		
}
