package jUnitsTests;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import static org.junit.Assert.fail;

public class Page {

	static WebDriver driver;

	public static void verifyElementDisplayed(By by) {
		driver.findElement(by).isDisplayed();
		try {
			Assert.assertTrue("Element was not found", driver.findElement(by).isDisplayed());
		} catch (NoSuchElementException e) {
			fail("Element Not found");
		}
	}

	public static void verifyTextDisplayed(String text) {
		String xpath = "//*[.='" + text + "']";
		try {
			Assert.assertTrue("text was not displayed", driver.findElement(By.xpath(xpath)).isDisplayed());
		} catch (NoSuchElementException e) {
			fail(text + " not found ");

		}

	}
	
	
	public static void switchWindow(String title, WebDriver driver) {
		String original = driver.getWindowHandle();
		for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
			if (title.equals(driver.getTitle())) {
				return;
			}
		}
		driver.switchTo().window(original);
	}

}











