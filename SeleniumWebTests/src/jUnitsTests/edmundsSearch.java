package jUnitsTests;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class edmundsSearch {

	WebDriver driver;

	@Before
	public void setUp() throws Exception {

  System.setProperty("webdriver.chrome.driver","C:\\Users\\Parahat\\Documents\\Libraries\\drivers\\chromedriver.exe");
		
	driver = new ChromeDriver();

	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
	driver.manage().window().maximize();
	driver.get("https://www.edmunds.com");

	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void test() {

		WebElement usedCars = driver.findElement(By.xpath("//a[@href='/used-cars-for-sale/']"));
		usedCars.click();

		WebElement zipcode = driver.findElement(By.name("zipcode"));
		zipcode.clear();
		zipcode.sendKeys("22102");

		Select make = new Select(driver.findElement(By.name("make")));
		make.selectByVisibleText("Acura");
		
		String strMake=make.getFirstSelectedOption().getText();
		System.out.println(strMake);
		
		int count = make.getOptions().size();
		System.out.println("car make count:"+count);
		

		Select model = new Select(driver.findElement(By.name("model")));
		// model.selectByIndex(5);
		model.selectByValue("MDX");
		String strModel = model.getFirstSelectedOption().getText();
		System.out.println(strModel);
		
		//span[contains(text(),'Start Shopping')]
		
		WebElement startShopping=driver.findElement(By.xpath("//span[contains(text(),'Start Shopping')]"));
		startShopping.click();
		
		WebElement cpo=driver.findElement(By.id("inv-type-cpo"));
		cpo.click();
		
		
		

	}

}
