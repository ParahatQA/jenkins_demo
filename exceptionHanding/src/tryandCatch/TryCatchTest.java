package tryandCatch;

public class TryCatchTest {

	public static void main(String[] args) {

		int b = 8;
		int c = 0;

		try {
			
			String[] jobIds = new String[4];

			jobIds[0] = "BA";
			jobIds[1] = "QA";
			jobIds[2] = "DEV";
			jobIds[3] = "PO";
			jobIds[4] = "SM";

			for (String string : jobIds) {
				System.out.println(string);
			}
			
			
		} catch (IndexOutOfBoundsException e) {
 
			System.out.println("IndexOutOfBoundsException  exception");
			System.exit(0);
		}catch (Exception ex){
			System.out.println("General exception");
		}finally {
			System.out.println("I am final block");
		}
	}

}
