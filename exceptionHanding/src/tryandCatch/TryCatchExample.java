package tryandCatch;

public class TryCatchExample {

	public static void main(String[] args) {

		try {

			String[] jobIds = new String[4];

			jobIds[0] = "BA";
			jobIds[1] = "QA";
			jobIds[2] = "DEV";
			jobIds[3] = "PO";
			jobIds[4] = "SM";

			for (String string : jobIds) {
				System.out.println(string);
			}
			System.exit(0);
			
		} catch (IndexOutOfBoundsException index) {
			System.out.println("index out of bound exception");
			
		} catch (Exception e) {
			System.out.println("Exception");
		}finally {
			System.out.println("Finally");
		}
	}

}
