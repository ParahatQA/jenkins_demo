package tryandCatch;

public class ThrowStatement {
	
	static String username;
	
	public static void main(String[] args) {
		
		//throw new ArithmeticException();
		if(username==null){
			throw new RuntimeException("Username was  not passed");
		}
	}

}
