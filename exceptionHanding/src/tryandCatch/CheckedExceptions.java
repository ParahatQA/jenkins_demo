package tryandCatch;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CheckedExceptions {
	
	public static void main(String[] args) throws InterruptedException{
	
		
		
		
		
		sleep(1000);
		
		try{
			sleep(1000);
		}catch(InterruptedException e){
			e.printStackTrace();
		}
		
		
		try{
			Thread.sleep(2000);
		}catch(Exception e){
			System.out.println("Exception happened, thread is not sleeping");
		}
	
	
	}
	
	public static void sleep(int milliseconds) throws InterruptedException{
		Thread.sleep(milliseconds);
	}
	
}