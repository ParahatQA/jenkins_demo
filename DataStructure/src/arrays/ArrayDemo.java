package arrays;

import java.util.FormatFlagsConversionMismatchException;

public class ArrayDemo {

	public static void main(String[] args) {

		 String[] fruits=new String[6];

		 fruits[0]="apples";
		 fruits[1]="kiwi";
		 fruits[2]="grapes";
		 fruits[3]="orange";
		 fruits[4]="mango";
		 fruits[5]="watermelon";

		 System.out.println("Fruit at Element 1: "+fruits[0]);
		 System.out.println("Fruit at Element 2: "+fruits[1]);
		 System.out.println("Fruit at Element 3: "+fruits[2]);
		 System.out.println("Fruit at Element 4: "+fruits[3]);
		 System.out.println("Fruit at Element 5: "+fruits[4]);
		 System.out.println("Fruit at Element 6: "+fruits[5]);

		 
		 fruits[0]="banana"; //  change  the  value of item
		 
		 System.out.println("I like "+fruits[0] +"," +fruits[1]+ "and"+fruits[5]);
		 
		 String[] fruit={"apples","kiwis","grapes","orange","mango","watermelon"};
			
		 System.out.println(fruits.length);
		
}}
