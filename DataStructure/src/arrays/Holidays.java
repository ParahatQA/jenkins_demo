package arrays;

import java.util.ArrayList;
import java.util.List;


public class Holidays {
	
	private List<String>holidays;
	
	public void addHolidays(){
		
		holidays=new ArrayList<>();
		
		holidays.add("New Year");
		holidays.add("Martin Luther King");
		holidays.add("Presidents");
		holidays.add("Memorial  ");
		holidays.add("Mothers");
		holidays.add("Fathers");
		holidays.add("Thanksgiving");
		holidays.add("Christmas");
	}
	
	
	public boolean isHoliday(String holidayName){
		
		return holidays.contains(holidayName);
	}	
	
		
	
}
