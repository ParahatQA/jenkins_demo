package arrays;

public class Garage {
	public static void main(String[] args) {
		
		Car car1= new Car("Ferary", 8);
		Car car2= new Car("BMW", 6);
		Car car3= new Car("mERC", 1);
		Car car4= new Car("Honda", 5);
		Car car5= new Car("Toyota", 16);
		
		// declare an  array
		// strat with  type and add [] and give it a  name
		
		Car[] myCars={car1,car2,car3,car4,car5};
				
		for (int i = 0; i < myCars.length; i++) {
			System.out.println("Car model is "+myCars[i].getCarModel()+", "+"Car cylinder count is "+myCars[i].getCylinderCount());
		}
		
		
		
//		Car[] myCars1= new Car[5]; 
//		
//		myCars1[0]=car1;
//		myCars1[1]=car2;
//		myCars1[2]=car3;
//		myCars1[3]=car4;
//		myCars1[4]=new Car ("Lexus",6);
//		
//		for(Car eachCar: myCars1){
//			System.out.println("For each loop car  model is "+eachCar.getCarModel()+"car cylinder count is :"+eachCar.getCylinderCount());
//		}
		
		
		
		
		
	}
	
	

}
