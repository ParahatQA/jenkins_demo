package arrays;

import java.util.ArrayList;
import java.util.List;

public class AddNumbers {
	
	
	public static void main(String[] args) {
		int[] myInts={32,54,65,23,64,76,324,12,543,78,123,5434};
		System.out.println(getSum(myInts));
		int[] yourInts= new int[1];
		yourInts[0]=100;
		yourInts[1]=-100;
		System.out.println(getSum(yourInts));
		
		List<Integer>myIntList= new ArrayList<>();
		
		myIntList.add(123);
		myIntList.add(567);
		
		int sum=getSum(myIntList);
		System.out.println(sum);
		
		myIntList.add(10);
		sum=getSum(myIntList);
		System.out.println(sum);
			
	}
	
	public static int getSum(int [] nums){
		int sum=0;
		for(int num: nums){
			sum+=num;
		}
		return sum;
	}
	
	
	public static int getSum(List<Integer> nums){
		int sum=0;
		for(int num: nums){
			sum+=num;
		}
		return sum;
		
		
	}
	
	
	

}
