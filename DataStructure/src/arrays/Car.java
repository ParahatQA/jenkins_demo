package arrays;

public class Car {
	
	private String carModel;
	private int cylinderCount;
	
	
	public Car(){
		
	}
		
	
	public Car(String modal, int count){
		this.carModel=modal;
		this.cylinderCount=count;
	}


	public String getCarModel() {
		return this.carModel;
	}


	public int getCylinderCount() {
		return cylinderCount;
	}


	
	

}
