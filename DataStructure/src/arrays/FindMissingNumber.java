package arrays;

import java.util.Arrays;

public class FindMissingNumber {

	// int[] array = {1,2,3,5,6};
	// int missingNumber=getMissingNumber(array,6);
	// System.out.printf("Missing Number in array %s is %d
	// %n",Arrays.toString(array),missingNumber);
	//
	// }
	//
	//
	//
	// private static int getMissingNumber(int [] array, int n){
	// int actualSum=0;
	// int expectedSum=(n*(n+1)/2);
	// for (int i : array) {
	// actualSum=actualSum+i;
	//
	// }
	// return expectedSum-actualSum;
	// }

	public static void main(String[] args) {
		int[] a = { 1, 2, 5, 6, 8 };
		missingArray(a);
	}

	public static void missingArray(int[] givenArray) {

		for (int j = 1; j < 10; j++) {

			boolean inside = false;

			for (int i = 0; i <= givenArray.length - 1; i++) {
				if (givenArray[i] == j) {
					inside = true;
				}
			}
			if (inside != true) {
				System.out.println(j);
			}
		}

	}

}
