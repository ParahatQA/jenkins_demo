package arrays;

public class Vehicle {
	private String vehicleType;
	private  int mileage;
	
	
	public Vehicle(String aType, int aMileage){
	this.vehicleType=aType;
	this.mileage=aMileage;
		
	}


	public String getVehicleType() {
		return vehicleType;
	}

	public int getMileage() {
		return mileage;
	}

	@Override
	public String toString() {
	
		return "Vehicle type: +"+vehicleType + "mileage: "+mileage;
	}

	public Vehicle(){
		
	}
	

}
