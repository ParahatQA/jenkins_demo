package arrays;

public class CompareTwoArray {

	public static void main(String[] args) {

		int[] a = { 1, 2, 3, 5, 6 };
		int[] b = { 1, 2, 3, 5, 6, 7 };

	}

	public static boolean sameArrayOrNot(int[] a, int[] b) {
		if (a.length == b.length) {

			for (int i = 0; i < a.length - 1; i++) {

				if (a[i] != b[i]) {
					return false;

				}

			}
			return true;
		}
		return false;

	}

}
