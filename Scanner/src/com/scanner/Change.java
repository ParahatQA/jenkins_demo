package com.scanner;

import java.util.Scanner;

public class Change {
	public static void main(String[] args) {
		
		// Create the Scanner
		Scanner scanner = new Scanner(System.in);
		
		// Number  of  Pennies
		System.out.println("Enter  then  number  opf  pennies:");
		int numberOfPennies=scanner.nextInt();
		
	    // Number  of  Nickles
		System.out.println("Enter  then  number  opf  Nickles:");
		int numberOfNickles=scanner.nextInt();
		
		// Number  of  Dimes
		System.out.println("Enter  then  number  opf  Dimes:");
		int numberOfDimes=scanner.nextInt();
		
		// Number  of  Quarters
		System.out.println("Enter  then  number  opf  Quarters:");
		int numberOfQuarters=scanner.nextInt();
		
		// Close  the  scanner
		scanner.close();
		
		// calculate the  total
		int totalCents = numberOfPennies+
						numberOfNickles*5+
						numberOfDimes*10+
						numberOfQuarters*25;
		
		double  dollarValue=totalCents/100.0;
		
		System.out.println("Total cents: "+totalCents );
		System.out.println("Total dollar  value in  change  jar: $"+dollarValue);

		
		
	}

}
