package tests.contacts;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.HomePage;
import utilities.Configuration;

public class PhoneNumberTest {

	WebDriver driver;

	String callUsNowExpectedTxt = "Call us now: 0123-456-789";
	String expectedPhoneNum = "0123456789";

	@BeforeClass
	public void setup() {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Home\\Documents\\selenium dependencies\\drivers\\chromedriver.exe");

		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(Configuration.getProperty("url"));

	}

	@AfterClass
	public void tearDown() {

	}

	// @Test(priority = 0)
	// public void phoneNumberAreaTest() {
	// HomePage homepage = new HomePage(driver);
	// String actualCallTxt=homepage.callUsNow.getText();
	//
	//
	// assertTrue(homepage.callUsNow.isDisplayed());
	// assertEquals(homepage.callUsNow.getText(), callUsNowExpectedTxt);
	//
	// assertEquals(StringUtility.extractNumber(actualCallTxt), expectedPhoneNum);
	//
	// String actualPhoneNum= StringUtility.extractNumber(actualCallTxt);
	// assertEquals(actualPhoneNum, expectedPhoneNum);
	// //assertEquals(Long.parseLong(actualPhoneNum));
	//
	// }

	//@Test(priority = 0)
	public void priceTest() {
		HomePage homepage = new HomePage(driver);
		List<WebElement> prices = homepage.allPrices;
		System.out.println("original list  size "+prices.size());
		
//		Predicate<WebElement> emptyElementRemover=new Predicate<WebElement>() {
//
//			@Override
//			public boolean test(WebElement t) {
//				
//				return t.getText().length()!=0;
//			}
//			
//		};
		
		Predicate<WebElement> emptyElementRemover= t -> t.getText().length()!=0;
		prices.removeIf(emptyElementRemover);
		System.out.println("New list size after removeIf "+ prices.size());
		
		
		
		//prices.removeIf(filter);

//		for (int i = 0; i < prices.size(); i++) {
//
//			String priceTxt = prices.get(i).getText();
//			if (priceTxt.length() != 0) {
//				System.out.println(prices.get(i).getText());
//			}
//
//		}

	}
	
	@Test(priority = 0)
	public void priceTest1() {
		HomePage homepage = new HomePage(driver);
		List<WebElement> prices = homepage.allPrices;
		System.out.println("original list  size "+prices.size());
		
		for (int i = 0; i < prices.size(); i++) {
			String priceTxt=prices.get(i).getText();
			//System.out.println(priceTxt.substring(1));
			
			double priceAsNumber= Double.parseDouble(priceTxt.substring(1));
			if(priceAsNumber>30) {
				System.out.println(priceAsNumber);
			}
			
		}
		

	}
	

}
