package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	
	
private WebDriver driver;
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText="//span[@class='shop-phone']")
	public WebElement callUsNow;
	
	//@FindBy(className="content_price")
	//@FindBy(xpath="(//div[@class='right-block'])//span[@itemprop='price']\r\n" +"\r\n" +"Enter message)//span[@itemprop='price']")
	@FindBy(xpath="//ul[@id='homefeatured']/li//div[@class='right-block']//span[@class='price product-price']")


	public List<WebElement> allPrices;
	
	@FindBy(linkText="Sign in")
	public WebElement signInLink;
	
	@FindBy(xpath="//img[@class='logo img-responsive']")
	public WebElement logo;

	
	
	public boolean isAt() {
		return driver.getTitle().equals("My Store");
	}
	
	public void gotoLoginPage() {
		signInLink.click();
	}

}
