package utilities;

public class StringUtility {

	public static void main(String[] args) {

		extractNumber("Cybertek003Tech112ABC");
		extractNumber("Call us now: 0123-456-789");

	}

	public static String extractNumber(String targetString) {
		// String employeeID = "Cybertek003Tech112ABC";

		String onlyNumber = "";

		for (int i = 0; i < targetString.length(); i++) {

			char eachChar = targetString.charAt(i);

			if (Character.isDigit(eachChar)) {

				// System.out.print(eachChar+" ");
				onlyNumber = onlyNumber + eachChar;
			}
		}

		System.out.println(onlyNumber);

		return onlyNumber;

	}

}
