package tesla;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class HomePageStepDefinitions {
	
	WebDriver driver;
	
	@Given("^I am on the home page$")
	public void i_am_on_the_home_page() {
	   System.out.println("I am going to the www.tesla.com");
	   WebDriverManager.chromedriver().setup();
	   driver= new ChromeDriver();
	   driver.get("https://www.tesla.com/");
	   
	   
	}

	@When("^I click on the model S link$")
	public void i_click_on_the_model_S_link() {
	   System.out.println("Clicked on the Model S link");
	   driver.findElement(By.xpath("//nav[@class='tsla-header-nav']")).click();
	}

	@Then("^Model S homepage should be  displayed$")
	public void model_S_homepage_should_be_displayed() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}


}
