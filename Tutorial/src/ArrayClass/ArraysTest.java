package ArrayClass;

public class ArraysTest {

	public static void main(String[] args) {

		int[] myArray = new int[10];

		for (int i = 0; i < myArray.length; i++) {
			myArray[i] = i * 10;
		}
		printArray(myArray);
	}

	public static void printArray(int[] array) {

		for (int i = 0; i < array.length; i++) {
			System.out.println("Element " + i + " value is " + array[i]);
		}
	}
}
